package Azubi;

public class Angestellter extends Mitarbeiter{

	private static final int MAX_STUFE = 5;
	private int stufe;
	
	public Angestellter(String nachname, String vorname, double gehalt) {
		super(nachname, vorname, gehalt);
		this.stufe = 0;
	}
	
	// Stufe um 1 erhoehen
	public void befoerdere() {
		if (stufe < MAX_STUFE) {
			stufe++;
		} else {
			System.out.println("Es wurde die maximale Stufe erreicht.");
		}
	}

	// ausgabe aller Variableninhalte
	public void zeigeDaten() {
		super.zeigeDaten();
		System.out.println("  > Stufe: " + stufe);
	}

	// if (stufe > 1) gehaltserhoehung
	@Override
	public void addZulage(double betrag) {
		if (stufe > 1) {
			super.erhoeheGehalt(betrag);
		}		
	}

}