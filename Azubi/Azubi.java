package Azubi;

public class Azubi extends Mitarbeiter {

	private int abgelegtePruefungen;
	
	public Azubi(String nachname, String vorname, double gehalt) {
		super(nachname, vorname, gehalt);
		this.abgelegtePruefungen = 0;
	}
	
	// zahl der abgelegten pruefungen setzen
	public void setPruefungen(int anzahl) {
		this.abgelegtePruefungen = anzahl;
	}
	
	// ausgabe aller variableninhalte
	public void zeigeDaten() {
		super.zeigeDaten();
		System.out.println("  > Abgelegte Pruefungen: " + abgelegtePruefungen);
	}
	
	// if (abgelegtePruefungen > 3) Gehaltserhoehung
	@Override
	public void addZulage(double betrag) {
		if (abgelegtePruefungen > 3) {
			super.erhoeheGehalt(betrag);
		} else {
			System.out.println("Es wurden noch nicht mehr als 3 Pruefungen abgelegt.");
		}
	}
	
}
