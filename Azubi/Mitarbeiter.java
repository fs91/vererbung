package Azubi;

public abstract class Mitarbeiter {
	
	protected String nachname;
	protected String vorname;
	protected double gehalt;
	
	public Mitarbeiter(String nachname, String vorname, double gehalt) {
		this.nachname = nachname;
		this.vorname = vorname;
		this.gehalt = gehalt;
	}
	
	// erhoehe gehalt um Betrag
	public void erhoeheGehalt(double betrag) {
		this.gehalt = this.gehalt + betrag;
	}
	
	// ausgabe aller variableninhalte
	public void zeigeDaten() {
		System.out.println("Mitarbeiter: " + nachname + ", " + vorname);
		System.out.println("  > Gehalt: " + gehalt);
	}
	
	// gehalt durch zulage erhoehen
	public abstract void addZulage(double betrag);
	
}
