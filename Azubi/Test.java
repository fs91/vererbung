package Azubi;

public class Test {

	public static void main(String[] args) {
		
		// Azubi
		System.out.println("Instanziierung von Azubi\n");
		Azubi azubi = new Azubi("Kasten", "Karsten", 800.00);
		azubi.zeigeDaten();
		
		System.out.println("\n\nPruefungen auf 3 setzen und Zulage von 100.00 hinzugeben\n"); // line break f�r uebersichtlichkeit
		azubi.setPruefungen(3);
		azubi.addZulage(100.00);
		azubi.zeigeDaten();
		
		System.out.println("\n\nPruefungen auf 4 setzen und Zulage von 100.00 hinzugeben\n"); // line break f�r uebersichtlichkeit
		azubi.setPruefungen(4);
		azubi.addZulage(100.00);
		azubi.zeigeDaten();
		
		System.out.println("\n-------------------------------------------------\n"); // line break f�r uebersichtlichkeit
		
		// Angestellter
		System.out.println("Instanziierung von Angestellter");
		Angestellter angestellter = new Angestellter("Lampe", "Lars", 2400.00);
		angestellter.zeigeDaten();
		
		System.out.println("\n\nStufe auf 1 setzen und Zulage von 100.00 hinzugeben\n");
		angestellter.befoerdere();
		angestellter.addZulage(100.00);
		angestellter.zeigeDaten();
		
		System.out.println("\n\nStufe auf 2 setzen und Zulage von 100.00 hinzugeben\n");
		angestellter.befoerdere();
		angestellter.addZulage(100.00);
		angestellter.zeigeDaten();
		
		System.out.println("\n\nMAX_STUFE von 5 testen\n");
		angestellter.befoerdere();
		angestellter.befoerdere();
		angestellter.befoerdere();
		angestellter.befoerdere();
		angestellter.zeigeDaten();
		
	}
	
}
