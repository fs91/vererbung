package Figuren;

public abstract class Figur {
	
	public abstract void zeichne();
	public abstract double getFlaeche();
	
}
