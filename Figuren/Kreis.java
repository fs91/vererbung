package Figuren;

public class Kreis extends Figur {

	protected double x;
	protected double y;
	protected double r;
	
	public Kreis(double x, double y, double r) {
		this.x = x;
		this.y = y;
		this.r = r;
	}
	
	@Override
	public void zeichne() {
		System.out.println("Mittelpunkt X/Y: " + this.x + " / " + this.y);
		System.out.println("Radius des Kreises: " + this.r);
	}

	@Override
	public double getFlaeche() {
		return Math.PI * (r * r);
	}

}
