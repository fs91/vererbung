package Figuren;

public class Rechteck extends Figur {

	protected double x;
	protected double y;
	protected double a;
	protected double b;
	
	public Rechteck(double x, double y, double a, double b) {
		this.x = x;
		this.y = y;
		this.a = a;
		this.b = b;
	}
	
	@Override
	public void zeichne() {
		System.out.println("Startpunkt X/Y: " + this.x + " / " + this.y);
		System.out.println("Laenge: " + this.a);
		System.out.println("Breite: " + this.b);
	}

	@Override
	public double getFlaeche() {
		return this.a * this.b;
	}

}
