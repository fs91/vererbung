package Figuren;

public class Test {

	public static void main(String[] args) {
		Figur[] figuren = new Figur[4];
		
		figuren[0] = new Kreis(0, 0, 5);
		figuren[1] = new Rechteck(0, 0, 100, 50);
		figuren[2] = new Kreis(2, 3, 10);
		figuren[3] = new Rechteck(1, 1, 5, 10);
		
		for (Figur f : figuren) {
			System.out.println("\n"); // line break f�r uebersichtlichkeit
			f.zeichne();
			f.getFlaeche();
		}
		
	}
	
}
