package Kuehlschrank;

public class Brot extends Speise{
	
	public Brot(int type, int menge) {
		super("", menge);
		switch (type) {
		case 0:
			this.name = "Weissbrot";
			break;
		case 1:
			this.name = "Schwarzbrot";
			break;
		case 2:
			this.name = "Mischbrot";
			break;
		default:
			this.name = "Spezialbrot";
			break;
		}
	}

}
