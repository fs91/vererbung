package Kuehlschrank;

public class Getraenk extends Lebensmittel{

	public Getraenk(String name, int menge) {
		super(name, menge);
	}

	@Override
	public boolean essen() {
		return false;
	}

	@Override
	public boolean trinken() {
		if (this.menge <= 0) {
			return false;
		} else {
			if (this.getClass().getSimpleName().contentEquals("Mate")) {
				this.menge = this.menge - 100;
			} else {
				this.menge = this.menge - 200;
			}
			return true;
		}
	}
	
	public void trinken(int menge) {
		
	}
	
}
