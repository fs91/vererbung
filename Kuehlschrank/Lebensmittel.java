package Kuehlschrank;

public abstract class Lebensmittel {

	protected String name;
	protected int menge;
	
	public Lebensmittel(String name, int menge) {
		this.name = name;
		this.menge = menge;
	}
	
	public abstract boolean essen();
	public abstract boolean trinken();
	
	public String status() {
		String platzhalter = "Klasse: " + this.getClass().getSimpleName() + ", Instanz: " + this.name + ", Menge: ";
		
		if (this.getClass().getSimpleName().contentEquals("Mate") || 
			this.getClass().getSimpleName().contentEquals("Wasser")) {
			platzhalter = platzhalter + this.menge + "ml"; 
		} else {
			platzhalter = platzhalter + this.menge + "g";
		}
		
		return platzhalter;
	}
	
}
