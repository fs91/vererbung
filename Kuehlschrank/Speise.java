package Kuehlschrank;

public class Speise extends Lebensmittel{

	public Speise(String name, int menge) {
		super(name, menge);
	}

	@Override
	public boolean essen() {
		if (this.menge <= 0) {
			return false;
		} else {
			if (this.getClass().getSimpleName().contentEquals("Brot")) {
				this.menge = this.menge - 50;
			} else {
				this.menge = this.menge - 10;
			}
			return true;
		}
	}

	@Override
	public boolean trinken() {
		return false;
	}
	
	public void essen(int menge) {
		
	}
	
}
