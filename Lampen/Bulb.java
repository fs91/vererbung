package Lampen;

public class Bulb extends Lamp {

	public Bulb(int watt) {
		super(watt);
	}

	@Override
	public String toString(int hoursPerDay) {
		return "A bulb consumes " + this.annualPowerConsumption(hoursPerDay) + " kWh per year.";
	}

	
	
}
