package Lampen;

public class LEDBulb extends Lamp {

	public LEDBulb(int watt) {
		super(watt);
	}

	@Override
	public String toString(int hoursPerDay) {
		
		return "A led bulb consumes " + this.annualPowerConsumption(hoursPerDay) + " kWh per year.";
	}

}
