package Lampen;

public abstract class Lamp {
	
	private int watt;
	
	public Lamp(int watt) {
		this.watt = watt;
	}
	
	protected int annualPowerConsumption(int hoursPerDay) {
		return (this.watt * hoursPerDay * 365) / 1000;
	}
	
	public abstract String toString(int hoursPerDay);
	
	public int getWatt() {
		return this.watt;
	}
	
}
